## Up & Running

1. `npm install`
2. `redis-server`
3. `PORT=3001 npm start`


## Client side example

```
var socket = io.connect('localhost:3001/myNamespace', { query: { me: "33333333333333333333a000" } });
socket.on('connect', function() {
  console.log('try connection with the server');
});
socket.on('connect_ACK', function(data) {
  console.log('connected ' + JSON.stringify(data));
});
socket.on('HI_THERE', function(data) {
  console.log('HI_THERE ' + JSON.stringify(data));
});
```

## Use case

1. Rise multiple servers
2. Connect multiple clients
3. Ask one of the servers to send a msg to one of the clients via HTTP, example: `curl http://localhost:3002/33333333333333333333a000`
