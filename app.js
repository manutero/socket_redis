'use strict'
const express = require('express')
const http = require('http')
const redis = require('redis')
const socketIO = require('socket.io')

// BUG ABIERTO EN SOCKET.IO-REDIS.... NO MOLA, PERO UN TIO HA HECHO UN FORK SIN EL BUG
// const socketIORedis = require('socket.io-redis')
const socketIORedis = require('nm.socket.io-redis')
const REDIS_OPTS = { return_buffers: true, host: 'localhost', port: 6379 }


console.log('starting...')


let app = express()
let server = http.createServer(app)
let io = socketIO(server)
let pubClient = redis.createClient(REDIS_OPTS)
let subClient = redis.createClient(REDIS_OPTS)
let patientNamespace = io.of('/myNamespace')

io.adapter(socketIORedis({ pubClient, subClient }))
io.use(authenticateMiddleware)
app.get('/:userId', _sendHelloToUser)
patientNamespace.on('connect', _newUser)
subClient.subscribe('HELLO_TO')
subClient.on('message', _onRedisEvent)
server.listen(process.env.PORT ||  3000, _onServerListening)


/* ------------------------------------------------------ */

function _onServerListening() {
  console.log(`listening at port ${server.address().port}`)
  console.log(`socket listening at ${patientNamespace.name}`)
}

function _sendHelloToUser(req, res, next) {
  let userId = req.params['userId']
  console.log(`sending HELLO_TO to ${userId}, creating a new event...`)
  pubClient.publish('HELLO_TO', userId)
  res.status(202).json({ target: userId, msg: 'HELLO_TO' })
}

function _newUser(socket) {
  let userId = socket.handshake.query.me
  pubClient.HSET('USERS', userId, socket.id, (err, response) => {
    if (!err) {
      console.log(`${userId} connected at ${socket.id}, cached him and sending "connect_ACK"`)
      socket.emit('connect_ACK', { userId })
    }
  })
}

function _doIKnowThatUser(u) { return u ? true : false }

function authenticateMiddleware(socket, next) {
  if (socket.handshake.query.me) { return next()  }
  next(new Error('Authentication error'))
}

function _onRedisEvent(event, message) {
  console.log('recieved ' + event + '... can I do smth?...')
  pubClient.HGET('USERS', message, (err, response) => {
    let socket = patientNamespace.sockets[response.toString()] // response: Buffer
    if (_doIKnowThatUser(socket)) {
      console.log('Sure I do! sending HI_THERE to the user')
      socket.emit('HI_THERE', {})
    } else {
      console.log('No i cannot, should be someone else...')
    }
  })
}